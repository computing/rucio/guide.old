# Removing datasets

## Removing replicas controlled by a rule

In the cases where replicas are controlled directly through replication rules,
removing a replica dataset simply requires setting a finite lifetime for the
rule.  Expedited removal can be acheived by setting the lifetime to a negative
value.

When the `rucio-judge-cleaner` daemon sees the rule expire, locks on the
affected files are released and the `rucio-reaper` daemon will work its way
through the dataset, removing files from the relevant RSEs.

!!! example "Remove the replica of `O3GK:G-G1_HOFT_C02_L3` from `LIGO-WA`"

    Find the rule-id for this dataset:

    <!-- markdownlint-disable MD013 -->
    ```console
    $  rucio list-rules --account root | grep O3GK:G-G1_HOFT_C02_L3 | grep LIGO-WA | awk '{print $1}'
    4e3bb2f2edfd4324891ae47d12e00b08
    ```

    Set the rule lifetime to `-1`:

    ```console
    $  rucio update-rule 4e3bb2f2edfd4324891ae47d12e00b08 --lifetime -1
    ```

    Check the `rucio-judge-cleaner` log:

    <!-- markdownlint-disable MD013 -->
    ```console
    $  kubectl logs prod-judge-cleaner-6cbd556d6d-mgvnw| grep O3GK | tail -1
    2021-02-11 19:04:17,470 7 DEBUG Deleting lock O3GK:G-G1_RDS_C02_L3-1271239560-60.gwf for rule 4e3bb2f2edfd4324891ae47d12e00b08
    ```

    Check the `rucio-hermes` log:

    ```console
    $  kubectl logs prod-reaper2-75898c9864-vtbvl| grep O3GK | tail -1
    2021-02-11 19:09:31,538 6 INFO  Thread [1/2] :  Deletion ATTEMPT of O3GK:G-G1_RDS_C02_L3-1270859580-60.gwf as gsiftp://ldas-lho.ligo-wa.caltech.edu:2811/archive/frames/O3GK/G1_HOFT_C02_L3/G-G1_RDS_C02_L3-12708/G-G1_RDS_C02_L3-1270859580-60.gwf on LIGO-WA
    ```

## Replacing datasets

Replacing an existing dataset is a two part process:

1. Remove all replica datasets
1. Update the the registered DIDs with the new files

Removal of replicas is straightforward, as above.

This second part is non-trivial: DIDs are unique and immutable objects with MD5
and Adler32 checksums which cannot be changed.  This is by design and is
intended to preserve data integrity and ensure sensible file versioning.

Supporting the practice of modifying files in-place requires modifying the
backend SQL database manually.  To modify existing files, we must either delete
and re-register the files; or we must edit the file checksums wherever they
appear.

### Delete and re-register

This is the easiest option.  To see what's involved, assume all replicas of a
dataset under rule management have been removed and we want to start over.
Note that in this procedure, we assume an "original" set of file replicas
which will *not* be removed; we just want to remove all trace of those files
from Rucio itself.

We will remove the following DIDs and the remaining replicas:

* dataset name: `O3GK:G-G1_HOFT_C02_L3`
* file names: `G-G1_RDS_C02_L3%`

SQL modification procedure:

1. Delete files from `replicas` table
1. Delete files from `did_meta` table
1. Delete files from `dids` table
1. Delete dataset from `contents` table
1. Delete dataset from `dids` table

We can then simply re-run the original data registration jobs.

!!! example "Completely remove DIDs from Rucio"

    Goal: remove all trace of dataset:

    ```console
    $  rucio list-dataset-replicas O3GK:G-G1_HOFT_C02_L3 --deep

    DATASET: O3GK:G-G1_HOFT_C02_L3
    +------------------+---------+---------+
    | RSE              |   FOUND |   TOTAL |
    |------------------+---------+---------|
    | LIGO-CIT-ARCHIVE |   19740 |   19740 |
    +------------------+---------+---------+
    ```

    Log into the SQL database and check the scope and file pattern yield expected files:

    ```sql
    rucio=# SELECT COUNT(*) FROM dids WHERE name LIKE 'G-G1_RDS_C02_L3%' AND scope='O3GK';
     COUNT 
    -------
     19740

     rucio=# SELECT COUNT(*) FROM replicas WHERE name LIKE 'G-G1_RDS_C02_L3%' AND scope='O3GK';
     COUNT
    -------
     19740
    (1 row)
    ```

    Purge replicas:

    ```sql
    rucio=# DELETE FROM replicas WHERE name LIKE 'G-G1_RDS_C02_L3%' AND scope='O3GK';
    DELETE 19740
    ```

    Purge DID metadata:

    ```sql
    rucio=# delete from did_meta where name like 'G-G1_RDS_C02_L3%' and scope='O3GK';
    DELETE 19739
    ```

    Purge DIDs:

    ```sql
    rucio=# delete from dids where name like 'G-G1_RDS_C02_L3%' and scope='O3GK';
    DELETE 19740
    ```

    Finally, purge the dataset DIDs from the `contents` table:

    ```sql
    rucio=# delete from contents where scope='O3GK' and name like 'G-G1_HOFT_C02_L3%';
    DELETE 1
    ```

    and then the `dids` table:

    ```sql
    rucio=# delete from dids where scope='O3GK' and name like 'G-G1_HOFT_C02_L3%';
    DELETE 1
    ```

    The DID `O3GK:G-G1_HOFT_C02_L3` and all its former members are now free for re-use.

### Modifying checksums
