# Replicating datasets

!!! Example "Check LFN2PFN algorithm"

    ```shell
    (gwrucio) bash-4.2# ./test_lfn2pfn K-K1_HOFT_C20-1270578656-32.gwf -s=O3GK \
    > -r=LIGO-CIT --true-pfn \
    > O3GK/K1_HOFT_C20/K-K1_HOFT_C20-127/K-K1_HOFT_C20-1270578656-32.gwf
    2020-11-05 19:40:06,688 81  INFO  Checking provided
    (O3GK/K1_HOFT_C20/K-K1_HOFT_C20-127/K-K1_HOFT_C20-1270578656-32.gwf) and
    derived PFN
    (O3GK/K1_HOFT_C20/K-K1_HOFT_C20-127/K-K1_HOFT_C20-1270578656-32.gwf) match
    2020-11-05 19:40:06,688 81  INFO  PFN & derived PFN match!
    2020-11-05 19:40:06,689 81  INFO  LFN2PFN ALGORITHM OK!
    ```
