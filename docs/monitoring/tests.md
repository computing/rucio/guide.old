# Automated tests

## Automatix functionality tests

The
[automatix](https://rucio.readthedocs.io/en/latest/man/rucio-automatix.html)
daemon periodically uploads temporary datasets of random files to an RSE. It is
used to continuously check that an RSE is reachable and operating as expected.

We also use the
[transmogrifier](https://rucio.readthedocs.io/en/latest/man/rucio-transmogrifier.html)
daemon to automatically create replication rules from pre-defined subscriptions
for these datasets to monitor RSE connectivity.

These datasets and rules are created with a finite lifetime, after which they
are automatically deleted by the
[reaper](https://rucio.readthedocs.io/en/latest/man/rucio-reaper.html) and
[judge-cleaner](https://rucio.readthedocs.io/en/latest/man/rucio-judge-cleaner.html)
daemons, respectively.

See all 3rd party transfer activity from Functional Tests
[here](https://ligo-rucio-grafana.nautilus.optiputer.net/d/C8EwBoTZk/transfer-dashboard?orgId=1&refresh=10s&var-binning=10m&var-groupby=dst-rse&var-activity=Functional%20Test&var-dstrse=All&var-dsttype=All&var-srcrse=All&var-srctype=All&var-protocol=All&from=1609795751941&to=1609882151941).

### Automatix configuration

As with all other daemons in our deployment, automatix is deployed as a
kubernetes pod, which executes from the [rucio-daemons
container](https://hub.docker.com/r/rucio/rucio-daemons).  

Automatix configuration is controlled through:

1. The `automatix` section of the global rucio configuration, stored in the
   `configs` table in the SQL database
1. An input file read from the command line.

!!! example "Automatix global configuration"

    Global configuration (e.g. sleep interval, dataset lifetime etc) can be set
    through the CLI or python API. To set e.g. `dataset_lifetime` through the CLI:
    ```shell
    $  rucio-admin config set \
         --section automatix \
         --option dataset_lifetime \
         --value 900
    ```
    To see the current configuration: 
    ```shell
    $  rucio-admin config get --section automatix
    [automatix]
    did_prefix=automatix
    account=root
    file_pattern=did_prefix-project-datatype-uuid
    sites=LIGO-WA-HASH
    set_metadata=True
    separator=-
    scope=TEST
    dataset_lifetime=900
    dataset_pattern=did_prefix-project-datatype-uuid
    sleep_time=1800
    ```
    Changes to this configuration require a service restart.

!!! example "Automatix input file"

    Dataset sizes and metadata are controlled through an input file, similar to:
    ```json
    {
      "type1": {
        "probability": 1,
        "nbfiles": 10,
        "filesize": 1000000,
        "metadata":
        {
          "project": "test",
          "datatype": "GWF",
          "prod_step": "automatix"
        }
      },
      "type2": {
        "probability": 1,
        "nbfiles": 10,
        "filesize": 1000000,
        "metadata":
        {
          "project": "test",
          "datatype": "SFT",
          "prod_step": "automatix"
        }
      }
    }
    ```
    In this case we define two datasets which differ only in their associated
    metadata.  This metadata is used later to define subscriptions.  The
    `probability` field can be used to randomise file creation.

    This input file is then used at the command line like
    ```shell
    $ rucio-automatix --input-file /opt/rucio/etc/automatix.json
    ```

The above configurations result in 2 datasets, each comprised of 10x 1MB files,
uploaded to the RSE `LIGO-WA-HASH` every 30 minutes.  The datasets are
automatically deleted after 15 minutes.

Note that Automatix only uploads data from the daemon to the specified RSE(s);
it does not test inter-site connectivity or FTS operation.

!!! warning "Automatix metadata and the `did_key_map` table"

    Note that the `did_key_map` SQL table must be initialised with the
    appropriate keys for automatix to work correctly.  Execute the
    [`tools/sync_meta.py`](https://github.com/rucio/rucio/blob/master/tools/sync_meta.py)
    script on a host with a full rucio installation to initialise this table.
    This task only needs to be peformed once.

### Subscriptions

We can monitor inter-site connectivity and 3rd party transfers with FTS using
subscriptions to Automatix datasets.

A "subscription" is defined by a set of metadata and sites to which datasets
with matching metadata should be replicated.  The
[transmogrifier](https://rucio.readthedocs.io/en/latest/man/rucio-transmogrifier.html)
identifies matching datasets and creates matching replication rules for the
relevant RSEs automatically.  The usual replication mechanisms (i.e. the Judge
and Conveyor daemon sets) then take over and submit transfer jobs for third
party transfer with FTS.

Subscriptions can be defined with a finite lifetime, after which the
subscription and rules created from that subscription will be removed and
replicas purged from matching RSEs.  

For continuous functional tests of FTS and RSE servers, we subscribe each RSE
to the automatix dataset(s).

See all 3rd party transfer activity from Functional Tests
[here](https://ligo-rucio-grafana.nautilus.optiputer.net/d/C8EwBoTZk/transfer-dashboard?orgId=1&refresh=10s&var-binning=10m&var-groupby=dst-rse&var-activity=Functional%20Test&var-dstrse=All&var-dsttype=All&var-srcrse=All&var-srctype=All&var-protocol=All&from=1609795751941&to=1609882151941).

!!! example "Subscription to an Automatix dataset"

    Our Automatix datasets are created in the `TEST` scope, and are associated
    with the `test` project and `GWF` and `SFT` datatypes.  To subscribe RSE
    `OZSTAR` to the Automatix `GWF` datatype defined above:
    <!-- markdownlint-disable MD013 -->
    ```shell
    $  rucio-admin subscription add test \
      '{"scope": ["TEST"], "project": ["test"], "datatype": ["GWF"]}' \
      '[{"copies": 1, "rse_expression": "OZSTAR", "activity": "User Subscriptions"}]' \
      'automatix test'
    ```
    where we have subscribed to all datasets in the `test` project of type `GWF`.

    The subscription can also be updated to e.g. modify the [RSE
    expression](https://rucio.readthedocs.io/en/latest/rse_expressions.html)
    and set a finite lifetime:
    ```shell
    rucio-admin subscription update test \
      '{"scope": ["TEST"], "project": ["test"], "datatype": ["GWF"]}' \
      '[{"copies": 1, "rse_expression": "OZSTAR|CNAF|ICRR-LVK", "activity": "Functional Test"}]' \
      'automatix test' --lifetime 600
    ```
    The `activity` field can be used as a filter on the monitoring dashboard
    and in the Conveyor daemons for fine-grained replication
    management.

    List existing subscriptions with:
    ```shell
    $  rucio-admin subscription list test
    root: test UPDATED
      priority: 3
      filter: {"project": ["test"], "scope": ["TEST"], "datatype": ["GWF"]}
      rules: [{"rse_expression": "OZSTAR|CNAF|ICRR-LVK", "copies": 1, "activity": "Functional Test"}]
      comments: automatix test
    ```
