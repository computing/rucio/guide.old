# Service and transfer monitoring

Services and transfer status and performance are monitored through a collection
of functionality tests and Grafana/Kibana dashboards.  This section describes
the deployment and configuration of these systems.

## Quick links

Where prompted, login: `guest/guest`

* Transfer status: [grafana transfer dashboard](https://ligo-rucio-grafana.nautilus.optiputer.net)
* Kubernetes resource consumption: [Nautilus grafana dashboard](https://grafana.nautilus.optiputer.net/d/85a562078cdf77779eaa1add43ccec1e/kubernetes-compute-resources-namespace-pods?orgId=1&refresh=10s&var-datasource=prometheus&var-cluster=&var-namespace=ligo-rucio)
* Database host metrics: [grafana host dashboard](https://ligo-rucio-grafana.nautilus.optiputer.net/d/100000033/rucio-database-host-metrics?orgId=1&refresh=1m)
* PostgreSQL metrics: [grafana postgres dashboard](https://ligo-rucio-grafana.nautilus.optiputer.net/d/W4u9iCrWz/postgresql-database-dashboard?orgId=1&refresh=1m)
* Dataset availability (WIP): [grafana dataset dashboard](https://ligo-rucio-grafana.nautilus.optiputer.net/d/XRL20gOMz/rucio-data-sets?orgId=1)
