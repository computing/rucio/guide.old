# Database initialisation

## Create schema

## Add users

## Define RSEs

## DID keymap

The `did_key_map` table provides metadata keys which can be used to define
subscriptions.  

!!! Danger "Datatype is a validated key for datasets"
    The `datatype` of a dataset is validated by `did_key_map`.  If provided,
    the `datatype` *must* match the possible values in this table.

!!! Example "Create the `did_key_map` table"
    Execute
    [/tools/sync_meta.py](https://git.ligo.org/rucio/igwn-rucio-kustomize/-/blob/master/server/container/tools/sync_meta.py)
    (included in the server container image) to configure a `did_key_map` table
    useful for IGWN applications.

    ```shell
    [root@prod-rucio-server-686b8458b9-b8pl8]# python /tools/sync_meta.py
    ```

    Check the `did_key_map` table in the SQL database:


    ```sql
    rucio=# select * from did_key_map;
        key    |       value       |         updated_at         |         created_at
    -----------+-------------------+----------------------------+----------------------------
     project   | LIGO              | 2020-11-18 04:08:12.728649 | 2020-11-18 04:08:12.728662
     project   | Virgo             | 2020-11-18 04:08:12.827162 | 2020-11-18 04:08:12.827175
     project   | GEO600            | 2020-11-18 04:08:12.890888 | 2020-11-18 04:08:12.8909
     project   | KAGRA             | 2020-11-18 04:08:12.95936  | 2020-11-18 04:08:12.959374
     prod_step | NoProdstepDefined | 2020-11-18 04:08:13.135533 | 2020-11-18 04:08:13.135544
     prod_step | user              | 2020-11-18 04:08:13.164007 | 2020-11-18 04:08:13.164018
     datatype  | GWF               | 2020-11-18 04:08:13.247912 | 2020-11-18 04:08:13.247924
     datatype  | SFT               | 2020-11-18 04:08:13.289711 | 2020-11-18 04:08:13.289728
    ```
