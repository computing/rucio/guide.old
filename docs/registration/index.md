# Registering new data

Rucio *currently* only manages centrally-curated instrument data: frame files
and SFTs.  

Data managed by Rucio must be registered in the backend database.  These
pages describe how to register new data sets.

Files are registered remotely using a domain-specific registration tool,
[`gwrucio_registrar`](registrar.md), which is deployed in kubernetes and
operates in one of two modes:

 1. [Online registration](online-instrument-data.md): the registrar monitors a
  site and registers new data in near-real time.  This allows Rucio to
  continuously replicate data from instrument sites to central archives (and
  beyond).
 1. [Offline registration](offline-instrument-data.md): the registrar is
  provided a batch of existing files which are registered in bulk.  This is
  useful for registration and replication of archival datasets and higher
  levels of data reduction or cleaning which may be performed as an offline
  batch process.

See [here](registrar.md) for a description of the registration deployment model
and `gwrucio_registrar`.
