# IGWN rucio guide

This site attempts to describe the following

- The Rucio deployment for administrators / operators.
- Data registration and management for IGWN operators and users.
- End-point configurations for site administrators.
- Data discovery and access for IGWN users.

This site is based on the [IGWN Computing
Guide](https://computing.docs.ligo.org/) whose authors we gratefully
acknowledge for providing template documentation.
