# Contributing to the IGWN Computing Guide

This page outlines the recommended procedure for reporting issues / giving
feedback on the computing guide, or proposed new or modified content.

## Reporting issues / feedback

To report issues, or post feedback, please open a ticket by navigating to

<https://git.ligo.org/computing/guide/-/issues/new>

## Making contributions

To make a contribution, please follow the fork and
[merge request](https://git.ligo.org/help/user/project/merge_requests/index.md)
[workflow](https://git.ligo.org/help/workflow/forking_workflow.md).
This will ensure that your changes do not break the rendering of the site, and
also adhere to the best practices of Markdown document style.

### 1. Make a fork (copy) of LALSuite

**You only need to do this once**

1. Go to the [repository home page](https://git.ligo.org/computing/guide),
1. click on the *Fork* button, that should lead you
   [here](https://git.ligo.org/computing/guide/-/forks/new),
1. select the namespace that you want to create the fork in, this will
   usually be your personal namespace.

If you can't see the *Fork* button, make sure that you are logged in by
checking for your account profile photo in the top right-hand corner of the
screen.

### Clone your fork

Clone your fork with

```console
git clone git@git.ligo.org:<namespace>/guide.git
```

and add the upstream repository as a remote:

```console
cd guide/
git remote add upstream git@git.ligo.org:computing/guide.git
```

### Making changes

All changes should be developed on a feature branch in order to keep them
separate from other work, thus simplifying the review and merge once the
work is complete.
The workflow is:

1. Create a new feature branch configured to track the `master` branch of the
   `upstream` repository:

   ```console
   git checkout -b my-new-feature upstream/master
   ```

   This command creates the new branch `my-new-feature`, sets up tracking the
   `upstream` repository, and checks out the new branch.
   There are other ways to do these steps, but this is a good habit since it
   will allow you to `fetch` and `merge` changes from `upstream/master`
   directly onto the branch.

1. Develop the changes you would like to introduce, using `git commit` to
   finalise a specific change.
   Ideally commit small units of change often, rather than creating one large
   commit at the end, this will simplify review and make modifying any changes
   easier.

   Commit messages should be clear, identifying which code was changed, and why.
   Common practice is to use a short summary line (<50 characters), followed
   by a blank line, then more information in longer lines.

1. Push your changes to the remote copy of your fork on <https://git.ligo.org>.
   The first `push` of any new feature branch will require the
   `-u/--set-upstream` option to `push` to create a link between your new
   branch and the `origin` remote:

   ```console
   git push --set-upstream origin my-new-feature
   ```

   Subsequent pushes can be made with

   ```console
   git push origin my-new-feature
   ```

1. Keep your feature branch up to date with the `upstream` repository by doing.

   ```console
   git checkout my-new-feature
   git pull --rebase upstream master
   git push --force origin my-new-feature
   ```

   If there are conflicts between `upstream` changes and your changes, you
   will need to resolve them before pushing everything to your fork.

### Open a merge request

When you feel that your work is finished, you should create a merge request
to propose that your changes be merged into the main (`upstream`) repository.

After you have pushed your new feature branch to `origin`, you should find a
new button on the [repository home page](https://git.ligo.org/computing/guide/)
inviting you to create a merge request out of your newly pushed branch.
(If the button does not exist, you can initiate a merge request by going to
the _Merge Requests_ tab on your fork website on git.ligo.org and clicking
_New merge request_)

You should click the button, and proceed to fill in the title and description
boxes on the merge request page.

Once the request has been opened, one of the maintainers will assign someone
to review the change.
There may be suggestions and/or discussion with the reviewer.
These interactions are intended to make the resulting changes better.
The reviewer will merge your request.

Once the changes are merged into the upstream repository, you should remove
the development branch from your clone using.

```console
git branch -d my-new-feature
```

A feature branch should *not* be repurposed for further development as this
can result in problems merging upstream changes.
