# IGWN rucio docs

This project uses [MkDocs](https://www.mkdocs.org) to build a
documentation website for using rucio in IGWN.

We thank the authors of the [IGWN Computing Guide](https://computing.docs.ligo.org/) for providing templates to build from.
